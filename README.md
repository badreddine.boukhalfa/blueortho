# blueOrtho

Exercice PHP - Calculette

On souhaite réaliser un service de calculette permettant d’évaluer l’aire et le périmètre des figures géométriques suivantes : Disque, Rectangle, Carré, Triangle quelconque, Triangle isocèle, Triangle rectangle, Triangle équilatéral, Couronne.

1) Expliquer en quelques phrases, les concepts suivants de la programmation objet : héritage, polymorphisme, méthodes virtuelles, classe abstraite, interface

2) Proposer une architecture de classe mettant en œuvre un ou plusieurs des concepts ci-dessus et répondant à notre problème.

3) Implémenter les classes en PHP. Le code doit pouvoir être exécuté avec PHP7, sans utiliser des librairies tierces au sein des fonctions / classes métier. Les fonctions mathématiques de base de PHP (sin, sqrt, log,…) sont disponibles.

4) Structurer, organiser et implémenter ce mini projet en mettant en œuvre les bonnes pratiques que vous jugez pertinentes. Justifier vos choix en expliquant les bénéfices attendus, les écueils évités… Les sujets suivants peuvent être envisagés : organisation du code, des fichiers, règles de nommage, …

Si vous le souhaitez, l’utilisation d’un framework pour la structuration est possible. Dans ce cas, fournir les éléments nécessaires à la mise en place (composer.json par exemple).

5) Prévoir une possibilité d’exécution démontrant le bon fonctionnement de l’ensemble.

Notes :

- Il n’est pas demandé d’implémenter une IHM web.

- Une interface minimale (console ou API) sera un plus.

