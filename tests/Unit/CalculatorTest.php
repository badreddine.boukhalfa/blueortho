<?php

namespace App\Tests\Unit;

use App\Service\Calculator;
use PHPUnit\Framework\TestCase;

class CalculatorTest extends TestCase
{

    const TRIANGLE_QUELCONQUE = 'TQ';
    const TRIANGLE_ISOCELE = 'TISO';
    const TRIANGLE_EQUILATÉRAL = 'TEQ';
    const TRIANGLE_RECTANGLE ='TREC';
    
   
    public function testCarre()
    {
        $calculator = new Calculator();
        $resultAir = $calculator->evalCarrer(4,'air');
        $resultPre = $calculator->evalCarrer(4,'perimetre');

        $this->assertEquals(16, $resultAir);
        $this->assertEquals(16, $resultPre);
    }

    public function testRectangle()
    {
        $calculator = new Calculator();
        $resultAir = $calculator->evalRectangle(4,3,'air');
        $resultPre = $calculator->evalRectangle(4,3,'perimetre');

        $this->assertEquals(12, $resultAir);
        $this->assertEquals(14, $resultPre);
    }


    public function testTriangle()
    {
        // ($BC * $AH)/ 2;
        $calculator = new Calculator();
        $resultAirTQ = $calculator->evalTriangle($AB=0,$AC=0,$BC=7,$AH=4,$typeEval='air',$type=self::TRIANGLE_QUELCONQUE);
        $resultAirTISO = $calculator->evalTriangle($AB,$AC,$BC=7,$AH=4,$typeEval='air',$type=self::TRIANGLE_ISOCELE);
        $resultAirTEQ = $calculator->evalTriangle($AB,$AC,$BC=7,$AH=4,$typeEval='air',$type=self::TRIANGLE_EQUILATÉRAL);
        $resultAirTREC = $calculator->evalTriangle($AB=14,$AC=0,$BC=28,$AH=0,$typeEval='air',$type=self::TRIANGLE_RECTANGLE);

        //***********************************************************************
        // vérifier si le calcule air de triangle est bonne!
        $this->assertEquals(14, $resultAirTQ);
        $this->assertEquals(14, $resultAirTISO);
        $this->assertEquals(14, $resultAirTEQ);
        $this->assertEquals(196, $resultAirTREC);
        //*********************************** 

        $resultPreTQ = $calculator->evalTriangle($AB=3,$AC=7,$BC=5,$AH=0,$typeEval='perimetre',$type=self::TRIANGLE_QUELCONQUE);
        $resultPreTISO = $calculator->evalTriangle($AB=5,$AC=5,$BC=3,$AH=0,$typeEval='perimetre',$type=self::TRIANGLE_ISOCELE);
        $resultPreTEQ = $calculator->evalTriangle($AB=3,$AC=3,$BC=3,$AH=0,$typeEval='perimetre',$type=self::TRIANGLE_EQUILATÉRAL);
        $resultPreTREC = $calculator->evalTriangle($AB=5,$AC=4,$BC=6,$AH=0,$typeEval='perimetre',$type=self::TRIANGLE_RECTANGLE);

        // vérifier si le calcule premetre de triangle est bonne!

        $this->assertEquals(15, $resultPreTQ);
        $this->assertEquals(13, $resultPreTISO);
        $this->assertEquals(9, $resultPreTEQ);
        $this->assertEquals(15, $resultPreTREC);
    }

    public function testDisque()
    {
        $calculator = new Calculator();
        $resultAir=$calculator->evalDisque($rayon=3,$typeEval='air');
        $this->assertEquals(28.26, $resultAir);
        $resultPre=$calculator->evalDisque($rayon=3,$typeEval='perimetre');
        $this->assertEquals(18.84, $resultPre);
    }

    public function testCouronne()
    {
        $calculator = new Calculator();
        $resultAir=$calculator->evalCouronne($R=5,$r=3,$typeEval='air');
        $this->assertEquals(50.24, $resultAir);
        $resultPre=$calculator->evalCouronne($R=5,$r=3,$typeEval='perimetre');
        $this->assertEquals(50.24, $resultPre);

    }

}