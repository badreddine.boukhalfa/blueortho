<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use App\Service\Calculator;

class DisqueCommand extends Command
{

    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:disque';

       /** @var Calculator */
       private $calculator;

    public function __construct(Calculator $calculator)
    {
        $this->calculator = $calculator;

        parent::__construct();
    }

    protected function configure()
    {
        $this
        // the short description shown while running "php bin/console list"
        ->setDescription('calculate the area and perimeter.')
        // the full command description shown when running the command with
        // the "--help" option
        ->setHelp('This command allows you to calculate the area and perimeter...')
        //->addArgument('cote', InputArgument::REQUIRED, ' rensigner le cote svp')
    ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $helper = $this->getHelper('question');
        $question = new Question(
        'Pour calcuer l\'aire taper 1, 
        pour calculer perimetre taper 2 : ',0
        );
        $typeEval =$helper->ask($input, $output, $question);
        if ($typeEval==1 || $typeEval==2 )
        {
            $question = new Question('rensigner le rayon svp : ',0);
            $rayon =$helper->ask($input, $output, $question);
            if ($typeEval==1)
            {
                $output->writeln([
                    '===== air Disque  =====',
                    $this->calculator->evalDisque($rayon,$typeEval='air'),
                ]);
            }
            else
            {
                $output->writeln([
                    '===== perimetre Disque  =====',
                    $this->calculator->evalDisque($rayon,$typeEval='perimetre'),
                ]);

            }       

        }else { $output->writeln('vous devez taper 1 ou 2'); }
    }

   
}