<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;

class CalculatorCommand extends Command
{

    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:calculator';

    protected function configure()
    {
        $this
        // the short description shown while running "php bin/console list"
        ->setDescription('calculate the area and perimeter.')

        // the full command description shown when running the command with
        // the "--help" option
        ->setHelp('This command allows you to calculate the area and perimeter...')
    ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        
        $helper = $this->getHelper('question');
        $question = new ChoiceQuestion(
        'choisissez une figure géométriques pour calculer son  aire  et primetre (la figure par defaut est: carrer) :',
        // choices can also be PHP objects that implement __toString() method
        ['Carre', 'Rectangle', 'Disque','Triangle','Couronne'],
        0
        );
        $question->setErrorMessage('le choix de la figure %s est invalide.');
        $figure = $helper->ask($input, $output, $question);

        switch ($figure) 
        {
            case 'Carre':
                $command = $this->getApplication()->find('app:carrer');
                $command->run($input,$output);
                break;
            case 'Rectangle':
                $command = $this->getApplication()->find('app:rectangle');
                $command->run($input,$output);
                break;
            case 'Disque':
                $command = $this->getApplication()->find('app:disque');
                $command->run($input,$output);
                break;
            case 'Couronne':
                $command = $this->getApplication()->find('app:couronne');
                $command->run($input,$output);
                break;    
            case 'Triangle':
                $command = $this->getApplication()->find('app:triangle');
                $command->run($input,$output);
                break;        
        }
    }
}