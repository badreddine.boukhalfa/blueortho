<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Question\ChoiceQuestion;
use App\Service\Calculator;

class TriangleCommand extends Command
{
    const TRIANGLE_QUELCONQUE = 'TQ';
    const TRIANGLE_ISOCELE = 'TISO';
    const TRIANGLE_EQUILATÉRAL = 'TEQ';
    const TRIANGLE_RECTANGLE ='TREC';

    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:triangle';

       /** @var Calculator */
       private $calculator;

    public function __construct(Calculator $calculator)
    {
        $this->calculator = $calculator;

        parent::__construct();
    }

    protected function configure()
    {
        $this
        // the short description shown while running "php bin/console list"
        ->setDescription('calculate the area and perimeter.')
        // the full command description shown when running the command with
        // the "--help" option
        ->setHelp('This command allows you to calculate the area and perimeter...')
        //->addArgument('cote', InputArgument::REQUIRED, ' rensigner le cote svp')
    ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $helper = $this->getHelper('question');
        $question = new Question(
        'Pour calcuer l\'aire taper 1, 
        pour calculer perimetre taper 2 : ',0
        );
        $typeEval =$helper->ask($input, $output, $question);
        if ($typeEval==1 || $typeEval==2 )
        {
            if ($typeEval==1)
            {   
               $this->getAir($input,$output);
            }
            else
            {
               $this->getPrimetre($input,$output);
            }       

        }else { $output->writeln('vous devez taper 1 ou 2'); }
    }


    protected function getAir($input,$output)
    {

        $helper = $this->getHelper('question');
        $question = new ChoiceQuestion(
        'choisissez le type de Triangle (la figure par defaut est: Triangle_quelconque) :',
        // choices can also be PHP objects that implement __toString() method
        ['Triangle_quelconque', 'Triangle_isocele', 'Triangle_equilateral','Triangle_rectangle'],
        0
        );
        $question->setErrorMessage('le choix de type %s est invalide.');
        $figure = $helper->ask($input, $output, $question);

        switch ($figure) 
        {
            case 'Triangle_quelconque':
                $question = new Question('rensigner la longueurs de BC : ',0);
                $BC =$helper->ask($input, $output, $question);
                $question = new Question('rensigner la longueurs de AH : ',0);
                $AH =$helper->ask($input, $output, $question);
                return $output->writeln([
                    '===== perimetre Triangle quelconque  =====',
                    $this->calculator->evalTriangle($AB=0,$AC=0,$BC=$BC,$AH=$AH,$typeEval='air',$type=self::TRIANGLE_QUELCONQUE),
                ]);
                break;
            case 'Triangle_isocele':
                $question = new Question('rensigner la longueurs de BC : ',0);
                $BC =$helper->ask($input, $output, $question);
                $question = new Question('rensigner la longueurs de AH : ',0);
                $AH =$helper->ask($input, $output, $question);
                return $output->writeln([
                    '===== perimetre Triangle isocele  =====',
                    $this->calculator->evalTriangle($AB=0,$AC=0,$BC=$BC,$AH=$AH,$typeEval='air',$type=self::TRIANGLE_ISOCELE),
                ]);
                break;
            case 'Triangle_equilateral':
                $question = new Question('rensigner la longueurs de BC : ',0);
                $BC =$helper->ask($input, $output, $question);
                $question = new Question('rensigner la longueurs de AH : ',0);
                $AH =$helper->ask($input, $output, $question);
                return $output->writeln([
                    '===== perimetre Triangle equilateral  =====',
                    $this->calculator->evalTriangle($AB=0,$AC=0,$BC=$BC,$AH=$AH,$typeEval='air',$type=self::TRIANGLE_EQUILATÉRAL),
                ]);
                break;
            case 'Triangle_rectangle':
                $question = new Question('rensigner la longueurs de AB : ',0);
                $AB =$helper->ask($input, $output, $question);
                $question = new Question('rensigner la longueurs de BC : ',0);
                $BC =$helper->ask($input, $output, $question);
                return $output->writeln([
                    '===== perimetre Triangle rectangle  =====',
                    $this->calculator->evalTriangle($AB=$AB,$AC=0,$BC=$BC,$AH=0,$typeEval='air',$type=self::TRIANGLE_RECTANGLE),
                ]);
                break;    
            }        
       
    }

    protected function getPrimetre($input,$output)
    {

        $helper = $this->getHelper('question');
        $question = new Question('rensigner la longueurs de AB : ',0);
        $AB =$helper->ask($input, $output, $question);
        $question = new Question('rensigner la longueurs de AC : ',0);
        $AC =$helper->ask($input, $output, $question);
        $question = new Question('rensigner la longueurs de BC : ',0);
        $BC =$helper->ask($input, $output, $question);

        $helper = $this->getHelper('question');
        $question = new ChoiceQuestion(
        'choisissez le type de Triangle (la figure par defaut est: Triangle_quelconque) :',
        // choices can also be PHP objects that implement __toString() method
        ['Triangle_quelconque', 'Triangle_isocele', 'Triangle_equilateral','Triangle_rectangle'],
        0
        );
        $question->setErrorMessage('le choix de type %s est invalide.');
        $figure = $helper->ask($input, $output, $question);

        switch ($figure) 
        {
            case 'Triangle_quelconque':
                return $output->writeln([
                    '===== perimetre Triangle quelconque  =====',
                    $this->calculator->evalTriangle($AB=$AB,$AC=$AC,$BC=$BC,$AH=0,$typeEval='perimetre',$type=self::TRIANGLE_QUELCONQUE),
                ]);
                break;
            case 'Triangle_isocele':
                return $output->writeln([
                    '===== perimetre Triangle isocele  =====',
                    $this->calculator->evalTriangle($AB=$AB,$AC=$AC,$BC=$BC,$AH=0,$typeEval='perimetre',$type=self::TRIANGLE_ISOCELE),
                ]);
                break;
            case 'Triangle_equilateral':
                return $output->writeln([
                    '===== perimetre Triangle équilatéral  =====',
                    $this->calculator->evalTriangle($AB=$AB,$AC=$AC,$BC=$BC,$AH=0,$typeEval='perimetre',$type=self::TRIANGLE_EQUILATÉRAL),
                ]);
                break;
            case 'Triangle_rectangle':
                return $output->writeln([
                    '===== perimetre Triangle rectangle  =====',
                    $this->calculator->evalTriangle($AB=$AB,$AC=$AC,$BC=$BC,$AH=0,$typeEval='perimetre',$type=self::TRIANGLE_RECTANGLE),
                ]);
                break;    
        }

    }
      
}