<?php 

namespace App\Service;

use App\Service\CalculeAitpiremetre;

class Calculator extends CalculeAitpiremetre
{
    const EVAL_AIR = 'air';
    const EVAL_PERIMETRE = 'perimetre';
    const TRIANGLE_QUELCONQUE = 'TQ';
    const TRIANGLE_ISOCELE = 'TISO';
    const TRIANGLE_EQUILATÉRAL = 'TEQ';
    const TRIANGLE_RECTANGLE ='TREC';


     //évaluation de l’aire et le périmètre de carré

     /**
     * @param int $c
     * @param string $type
     * @return int 
     */
    public function evalCarrer($c,$type):int
    {
        if(self::EVAL_AIR == $type) {
            
            return $c * $c;
        }
        if(self::EVAL_PERIMETRE == $type) {
            
            return $c * $c;
        }
    }

    /**
     * @param int $i
     * @param int $l
     * @param string $type
     * @return int 
     */

    public function evalRectangle($i,$l,$type):int
    {
        if(self::EVAL_AIR == $type) {
            
            return $l * $i;
        }
        if(self::EVAL_PERIMETRE == $type) {
            
            return ($l +$i)*2;
        }
    }
    
    /**
     * @param int $AB
     * @param int $AC
     * @param int $BC
     * @param int $AH
     * @param string $typeEval
     * @param string $type
     * @return int 
     */
    public function evalTriangle($AB,$AC,$BC,$AH,$typeEval,$type):int
    {
        // calculer air triangle 
        if((self::EVAL_AIR == $typeEval && self::TRIANGLE_QUELCONQUE == $type) || (self::EVAL_AIR == $typeEval && self::TRIANGLE_ISOCELE == $type) || (self::EVAL_AIR == $typeEval && self::TRIANGLE_EQUILATÉRAL == $type) ) 
        {

            return ($BC * $AH)/ 2;

        }else 
        {
            if(self::EVAL_AIR == $typeEval &&  self::TRIANGLE_RECTANGLE == $type) 
            {
            
                return ($AB * $BC)/2;
            }

        }
        // calcule perimetre triangle
        if(self::EVAL_PERIMETRE == $typeEval) 
        {
        
            return $AB + $AC + $BC;
        }
    }

    /**
     * @param int $rayon
     * @param string $typeEval
     * @return float 
     */
    public function evalDisque($rayon,$typeEval):float
    {
        if(self::EVAL_AIR == $typeEval) 
        {

            return 3.14 * $rayon * $rayon ;

        }
        if(self::EVAL_PERIMETRE == $typeEval) 
        {
        
            return 2 * 3.14  * $rayon ;
        }


    } 

    /**
     * @param int $R
     * @param int $r
     * @param string $typeEval
     * @return float 
     */
    public function evalCouronne($R,$r,$typeEval):float
    {
        if(self::EVAL_AIR == $typeEval) 
        {

            return 3.14 * (($R * $R) - ($r * $r)) ;
        }       
        if(self::EVAL_PERIMETRE == $typeEval) 
        {
        
            return 2 * 3.14 * ($R + $r);
        }
        
    }


}