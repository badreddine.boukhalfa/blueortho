<?php 

namespace App\Service;

abstract class CalculeAitpiremetre
{
   
    abstract public function evalCarrer($c,$type);
    
    abstract public function evalRectangle($i,$l,$type);

    abstract public function evalTriangle($AB,$AC,$BC,$AH,$typeEval,$type);

    abstract public function evalDisque($rayon,$typeEval);

    abstract public function evalCouronne($R,$r,$typeEval);

}